/*************************************/
var currentDate = new Date();
var sTime = currentDate.getTime();
var APIservice = "http://27.254.169.204"
/*************************************/

function onloadDataPaitentAppointment() {
    var datasend = new Object();
    var arrSetDataForCalendar =[];

    var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

    datasend.doctor_id = "";
    datasend.appointment_date = "";

    $.ajax({
        url: APIservice + "/api/Doctor/Appointment",
        //url: "http://10.88.40.162/api/Doctor/Appointment",
        type: "POST",
        data: JSON.stringify(datasend),
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        success: function (result) {
            var strRet = result;
            var setDataPaientHTML = "";

            // for (i = 0; i < strRet.length; i++) {
            //         setDataPaientHTML += "<div class='alert-notify info' style='padding-bottom: 10px;'>";
            //         setDataPaientHTML += "<div class='alert-notify-body' style='background-color: #dee2ee; border-radius: 7px;'>";
            //         setDataPaientHTML += "<div class='row gutters'>";
            //         setDataPaientHTML += "<div class='col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12'>";
            //         setDataPaientHTML += "<img src='images/patient_1.png' style='max-width: 60px; max-height: 60px;' alt=''/>";
            //         setDataPaientHTML += "</div>";
            //         setDataPaientHTML += "<div class='col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12'>";
            //         setDataPaientHTML += "<div class='alert-notify-title text-primary'>" + strRet[i].title + strRet[i].firstname + " " + strRet[i].lastname + "</div>";
            //         setDataPaientHTML += "<div class='alert-notify-text'>เลขที่อ้างอิง : " + strRet[i].ref_no + "</div>";
            //         setDataPaientHTML += "<div class='alert-notify-text'>อาการ : ปวดหัว</div>";
            //         setDataPaientHTML += "</div>";
            //         setDataPaientHTML += "</div>";
            //         setDataPaientHTML += "<div class='row gutters'>";
            //         setDataPaientHTML += "<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>";
            //         setDataPaientHTML += "<a href='operation.html?uaid=" + strRet[i].user_account_id + "&ptid=" + strRet[i].patient_id + "' class='btn btn-primary float-right' style='font-weight: 400; font-size: 12px;'>การตรวจรักษา</a>";
            //         setDataPaientHTML += "<a href='javascript:void(0);' class='btn btn-danger float-right' style='font-weight: 400; font-size: 12px; margin-right: 5px;'>แก้ไขนัด</a>";
            //         setDataPaientHTML += "</div>";
            //         setDataPaientHTML += "</div>";
            //         setDataPaientHTML += "<span class='type' style='background-color: #074b9c;'>สถานะ : " + strRet[i].status + "</span>";
            //         setDataPaientHTML += "</div>";
            //         setDataPaientHTML += "</div>";
            //     }

            // --------------------------- UPDATE BY MILO 24/07/2020 -------------------
            for (i = 0; i < strRet.length; i++) {
                setDataPaientHTML +=  " <ul class='header-notifications pb-2'>";
                setDataPaientHTML +=  " <li>";
                setDataPaientHTML +=  " <a href='appointment_details?uaid=" + strRet[i].user_account_id + "&ptid=" + strRet[i].patient_id + "' class='clearfix' style='padding: 0px;'>";
                setDataPaientHTML +=  " <div class='avatar'><img src='/images/user.png' alt='avatar'>";
                setDataPaientHTML +=  " <span class='noticfication-label"+[i]+"'></span>";
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " <div class='details'>";  
                setDataPaientHTML +=  " <div class='row gutters'>";
                setDataPaientHTML +=  " <div class='col-12 col-xl-7 col-lg-12 col-md-7 col-sm-7'>";
                setDataPaientHTML +=  " <p class='text-dark'><b  style='padding: 0px;'>" + strRet[i].title + strRet[i].firstname + " " + strRet[i].lastname + " ("+ strRet[i].ref_no + ")</b>";
                setDataPaientHTML +=  " <span class='icon-edit-2 pl-2' style='font-size: 17px; '></span>";
                setDataPaientHTML +=  " </p>";
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " <div class='col-12 col-xl-5 col-lg-12 col-md-5 col-sm-5 txtcenter'>";  
                setDataPaientHTML +=  " <span class='badge badge-info'>ตรวจรักษา</span>";
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " <div class='row gutters'>";
                setDataPaientHTML +=  " <div class='col-12 col-xl-7 col-lg-12 col-md-7 col-sm-7'>";
                setDataPaientHTML +=  " <p style='font-color:#999999;'>นัดมาเพื่อ : ติดตามอาการ และเจาะเลือดเพิ่มเติม</p>";
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " <div class='col-12 col-xl-5 col-lg-12 col-md-5 col-sm-5 txtcenter'>"; 
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " </div>";
                setDataPaientHTML +=  " </a>";
                setDataPaientHTML +=  " </li>";
                setDataPaientHTML +=  " </ul>";
            }
            // --------------------------- UPDATE BY MILO 24/07/2020 -------------------

            var setDataForCalendar = new Object();

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 1);
            setDataForCalendar.end = new Date(y, m, 2);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 3);
            setDataForCalendar.end = new Date(y, m, 4);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 5);
            setDataForCalendar.end = new Date(y, m, 6);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 8);
            setDataForCalendar.end = new Date(y, m, 9);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 13);
            setDataForCalendar.end = new Date(y, m, 14);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 17);
            setDataForCalendar.end = new Date(y, m, 18);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 20);
            setDataForCalendar.end = new Date(y, m, 21);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 25);
            setDataForCalendar.end = new Date(y, m, 26);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 28);
            setDataForCalendar.end = new Date(y, m, 29);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            setDataForCalendar = new Object();
            setDataForCalendar.title = 'นางสาวสุขภาพ แข็งแรง'
            setDataForCalendar.start = new Date(y, m, 30);
            setDataForCalendar.end = new Date(y, m, 31);
            setDataForCalendar.allDay = true
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);
            arrSetDataForCalendar.push(setDataForCalendar);

            //jQuery("#dataPaitentAppointment").html(setDataPaientHTML);

            // --------------------------- UPDATE BY MILO 24/07/2020 -------------------
            jQuery("#block-patient1").html(setDataPaientHTML);
            // jQuery("#block-patient2").html(setDataPaientHTML);
            // jQuery("#block-patient3").html(setDataPaientHTML);
            // jQuery("#block-patient4").html(setDataPaientHTML);
            // jQuery("#block-patient5").html(setDataPaientHTML);
            // jQuery("#block-patient6").html(setDataPaientHTML);
            // jQuery("#block-patient7").html(setDataPaientHTML);
            // jQuery("#block-patient8").html(setDataPaientHTML);
            // jQuery("#block-patient9").html(setDataPaientHTML);
            // --------------------------- UPDATE BY MILO 24/07/2020 -------------------

            // $('#calendar').fullCalendar({
            //     events: function(start, end, timezone, callback) {
            //         $.ajax({
            //             url: 'myxmlfeed.php',
            //             dataType: 'xml',
            //             data: {
            //                 // our hypothetical feed requires UNIX timestamps
            //                 start: start.unix(),
            //                 end: end.unix()
            //             },
            //             success: function(doc) {
            //                 var events = [];
            //                 $(doc).find('event').each(function() {
            //                     events.push({
            //                     title: $(this).attr('title'),
            //                     start: $(this).attr('start') // will be parsed
            //                     });
            //                 });
            //                 callback(events);
            //             }
            //         });
            //     }
            // });

            $('#AppointmentCalendar').fullCalendar({
                header: {
                    left: 'prev, next',
                    center: 'title',
                    right: 'today, month, agendaWeek, agendaDay'
                },
                //Add Events
                events: arrSetDataForCalendar,
                editable: false,
                eventLimit: true,
                droppable: false
            });
        }, 
        error: function (error) {
            console.log(error);
        }
    });
}

function changeReMed(getRemed) {
    switch(getRemed){
        case 1:
            if (jQuery("#chk_remed").prop("checked") == true) {
                jQuery("#case_remed").show();
            } else {
                jQuery("#case_remed").hide();
            }
        break;
        case 2:
            if (jQuery("#customCheck3").prop("checked") == true) {
                jQuery("#showCertificate").show();
            } else {
                jQuery("#showCertificate").hide();
            }
        break;
        default:
            break;
    }
    
}

function save_data() {
    alert("SAVE DATA!!");
}

function setDataBloodCompare() {
    jQuery("#list_data_2").css("display","block");
    jQuery("#table_data_1").css("display","none");
    jQuery("#table_data_2").css("display","block");
    jQuery(".data_blood_compare").modal('toggle');
}

function setShowImagesXRay(n) {
    var setStringShowImages = "";
    var setColtext = "";
    var allImages = 5;
    var classImages = "";

    if (n == 4) {
        setColtext = "6";
        classImages = "img_row2";
    } else {
        setColtext = "4";
        classImages = "img_row3";
    }
    
    setStringShowImages += "<div class='row gutters'>";
    for (var j = 1; j <= n; j++) {
        if (j <= allImages) {
            setStringShowImages += "<div class='col-xl-" + setColtext + " col-lg-" + setColtext + " col-md-" + setColtext + " col-sm-12 col-12'>";
            setStringShowImages += "<a href='/images/img_xray" + j + ".jpg' class='effects text-center'>";
            setStringShowImages += "<img src='/images/img_xray" + j + ".jpg' class='img-fluid " + classImages + "' style='object-fit: cover;' />";
            setStringShowImages += "<div class='overlay'>";
            setStringShowImages += "<span class='expand'>+</span>";
            setStringShowImages += "</div>";
            setStringShowImages += "</a>";
            setStringShowImages += "</div>";
        }
    }
    setStringShowImages += "</div>";

    jQuery("#setshowimages").html(setStringShowImages);
    onloadGallery();
}

function setShowImagesMRI(n) {
    var setStringShowImages = "";
    var setColtext = "";
    var allImages = 5;
    var classImages = "";

    if (n == 4) {
        setColtext = "6";
        classImages = "img_row2";
    } else {
        setColtext = "4";
        classImages = "img_row3";
    }

    setStringShowImages += "<div class='row gutters'>";
    for (var j = 1; j <= n; j++) {
        if (j <= allImages) {
            setStringShowImages += "<div class='col-xl-" + setColtext + " col-lg-" + setColtext + " col-md-" + setColtext + " col-sm-12 col-12'>";
            setStringShowImages += "<a href='/images/img_mri1.png' class='effects text-center'>";
            setStringShowImages += "<img src='/images/img_mri1.png' class='img-fluid " + classImages + "' style='object-fit: cover;' />";
            setStringShowImages += "<div class='overlay'>";
            setStringShowImages += "<span class='expand'>+</span>";
            setStringShowImages += "</div>";
            setStringShowImages += "</a>";
            setStringShowImages += "</div>";
        }
    }
    setStringShowImages += "</div>";
    
    jQuery("#setshowimages").html(setStringShowImages);
    onloadGallery();
}

function setShowImagesCTScan(n) {
    var setStringShowImages = "";
    var setColtext = "";
    var allImages = 5;
    var classImages = "";

    if (n == 4) {
        setColtext = "6";
        classImages = "img_row2";
    } else {
        setColtext = "4";
        classImages = "img_row3";
    }

    setStringShowImages += "<div class='row gutters'>";
    for (var j = 1; j <= n; j++) {
        if (j <= allImages) {
            setStringShowImages += "<div class='col-xl-" + setColtext + " col-lg-" + setColtext + " col-md-" + setColtext + " col-sm-12 col-12'>";
            setStringShowImages += "<a href='/images/img_ctscan1.png' class='effects text-center'>";
            setStringShowImages += "<img src='/images/img_ctscan1.png' class='img-fluid " + classImages + "' style='object-fit: cover;' />";
            setStringShowImages += "<div class='overlay'>";
            setStringShowImages += "<span class='expand'>+</span>";
            setStringShowImages += "</div>";
            setStringShowImages += "</a>";
            setStringShowImages += "</div>";
        }
    }
    setStringShowImages += "</div>";
    
    jQuery("#setshowimages").html(setStringShowImages);
    onloadGallery();
}

function onloadGallery() {
    if (typeof oldIE === 'undefined' && Object.keys) {
	    hljs.initHighlighting();
	    baguetteBox.run('.baguetteBoxOne');
	    baguetteBox.run('.baguetteBoxTwo');
	    baguetteBox.run('.baguetteBoxThree', {
		    animation: 'fadeIn',
	    });
        baguetteBox.run('.baguetteBoxFour', {
            buttons: false
        });
        baguetteBox.run('.baguetteBoxFive');
    }
}

function DeleteRowAddRemedLab(item) {
    var getNum = item.id.split("_");
    jQuery("#row_add_remed_lab_" + getNum[getNum.length - 1]).remove();
}
function login() {
    
    var user = $("#txt_username").val();
    var pass = $("#txt_password").val();
    if(user == ""){
        
        $("#error_massage_user").html("Please enter username");
        $("#error_massage_user").css("display", "block");

    }
    if(pass == ""){
        $("#error_massage_pass").html("Please enter password");
        $("#error_massage_pass").css("display", "block");
    }
    if(user != ""){
        $("#error_massage_user").html("");
        $("#error_massage_user").css("display", "none");
    }
    if(pass != ""){
        $("#error_massage_pass").html("");
        $("#error_massage_pass").css("display", "none");
    }
        var datasend = new Object();
        
        datasend.username = user;
        datasend.password = pass;
    if(user != "" && pass != ""){
        $.ajax({
                url: APIservice + "/api/Doctor/LoginByUsername",
                type: "POST",
                data: JSON.stringify(datasend),
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                success: function (result) {
                    var strRet = result;
                    sessionStorage.setItem("DoctorName", strRet.title + strRet.firstname + " " + strRet.lastname);
                    sessionStorage.setItem("doctor_id", strRet.doctor_id);
                    let id = sessionStorage.getItem("doctor_id");
                    switch(id){
                        case "1" :
                            window.location.href = "/Doctor/index";
                         break;
                        case "2" : 
                            window.location.href = "/Nurse/appointment";
                         break;
                        case "3" : 
                            window.location.href = "/Pharmarcist/index";
                         break;
                         case "4" : 
                            window.location.href = "/Personnel/index";
                         break;
                        default :
                         break;
                    }
                    
                },
                error: function (error) {
                    console.log(error);
                    $("#error_massage").html(error.responseText);
                    $("#error_massage").css("display","block");
                }
            });	
    }
            
}
function logout() {
    sessionStorage.removeItem("doctor_id");
    sessionStorage.removeItem("DoctorName");
   // window.location.href = "/login";
    var pathname = window.location.pathname;
    if ((pathname.indexOf("/Doctor") > -1)) {
        window.location.href = "/Doctor/login";
    } else if ((pathname.indexOf("/Nurse") > -1)) {
        window.location.href = "/Nurse/login";
    } else if ((pathname.indexOf("/Personnel") > -1)) {
        window.location.href = "/Personnel/login";
    } else if ((pathname.indexOf("/Pharmacist") > -1)) {
        window.location.href = "/Pharmacist/login";
    } else {
        window.location.href = "/login";
    }

}
function CalltoPatient() {
    var datasend = new Object();
    var urlParams = new URLSearchParams(window.location.search);
    var uaid = urlParams.get('uaid');
    var ptid = urlParams.get('ptid');
    var drid = sessionStorage.getItem("doctor_id");
    

    var d_yyyy = currentDate.getFullYear().toString();
    var d_MM = (((currentDate.getMonth() + 1) < 10 ? '0' : '') + (currentDate.getMonth() + 1)).toString();
    var d_dd = ((currentDate.getDate() < 10 ? '0' : '') + currentDate.getDate()).toString();
    var t_HH = ((currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours()).toString();
    var t_mm = ((currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes()).toString();
    var t_ss = ((currentDate.getSeconds() < 10 ? '0' : '') + currentDate.getSeconds()).toString();
    var dateRoom = d_yyyy + d_MM + d_dd + t_HH + t_mm + t_ss;

    if ((ptid != "" && ptid != null && ptid != undefined) && (drid != "" && drid != null && drid != undefined)) {
        datasend.user_account_id = uaid;
        datasend.doctor_id = drid;
        datasend.patient_id = ptid;
        datasend.streaming_id = dateRoom + drid + ptid;
        

        $.ajax({
            url: APIservice + "/api/Doctor/VideoCallToPatient",
            type: "POST",
            data: JSON.stringify(datasend),
            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            success: function (result) {
                window.open("https://demo.efinancethai.com/wowza/src/userPublish.html?" + dateRoom + drid + ptid, "_target");
                //window.open("wowza/src/userPublish.html?" + dateRoom + drid + ptid, "_target");
                jQuery(".call-patient").modal('toggle');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
   
}
function addProgram(status){ //---- เพิ่มโปรแกรมตรวจสุขภาพ-------
    var tempStatus = (status == 1) ? tempStatus = "1" : tempStatus = "";
    var selectProgramLab = jQuery("#selectProgramLab" + tempStatus);
    var completedTasks = jQuery("#completedProgramLab" + tempStatus);
    completedTasks.html("");
    selectProgramLab.find(".checkbox-laps").each(function(index) {
        if(jQuery('#' + this.id).prop('checked')) {
            var getId = this.id;
            var getTextLabel = jQuery('#' + getId).next('label.getProgramTxtFromLabel').text();
            completedTasks.append("- " + getTextLabel + "<br>");
        } 
    });

    jQuery("#btnAddNewLab" + tempStatus).click();
}
function onloadDataDoctorDailyPlan() {
    var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
    var y = date.getFullYear();
    
    var arrSetDataDoctorDailyPlanForCalendar =[];
    var setDataDoctorDailyPlanForCalendar = new Object();

    setDataDoctorDailyPlanForCalendar = new Object();
    setDataDoctorDailyPlanForCalendar.title = "อายุรกรรม"
    setDataDoctorDailyPlanForCalendar.start = new Date(y, m, 3);
    setDataDoctorDailyPlanForCalendar.end = new Date(y, m, 4);
    setDataDoctorDailyPlanForCalendar.allDay = true
    setDataDoctorDailyPlanForCalendar.backgroundColor = ""
    arrSetDataDoctorDailyPlanForCalendar.push(setDataDoctorDailyPlanForCalendar);

    setDataDoctorDailyPlanForCalendar = new Object();
    setDataDoctorDailyPlanForCalendar.title = "อายุรกรรม"
    setDataDoctorDailyPlanForCalendar.start = new Date(y, m, 10);
    setDataDoctorDailyPlanForCalendar.end = new Date(y, m, 11);
    setDataDoctorDailyPlanForCalendar.allDay = true
    arrSetDataDoctorDailyPlanForCalendar.push(setDataDoctorDailyPlanForCalendar);

    setDataDoctorDailyPlanForCalendar = new Object();
    setDataDoctorDailyPlanForCalendar.title = "อายุรกรรม"
    setDataDoctorDailyPlanForCalendar.start = new Date(y, m, 17);
    setDataDoctorDailyPlanForCalendar.end = new Date(y, m, 18);
    setDataDoctorDailyPlanForCalendar.allDay = true
    arrSetDataDoctorDailyPlanForCalendar.push(setDataDoctorDailyPlanForCalendar);

    setDataDoctorDailyPlanForCalendar = new Object();
    setDataDoctorDailyPlanForCalendar.title = "อายุรกรรม"
    setDataDoctorDailyPlanForCalendar.start = new Date(y, m, 24);
    setDataDoctorDailyPlanForCalendar.end = new Date(y, m, 25);
    setDataDoctorDailyPlanForCalendar.allDay = true
    arrSetDataDoctorDailyPlanForCalendar.push(setDataDoctorDailyPlanForCalendar);

    setDataDoctorDailyPlanForCalendar = new Object();
    setDataDoctorDailyPlanForCalendar.title = "อายุรกรรม"
    setDataDoctorDailyPlanForCalendar.start = new Date(y, m, 31);
    setDataDoctorDailyPlanForCalendar.end = new Date(y, (m + 1), 1);
    setDataDoctorDailyPlanForCalendar.allDay = true
    arrSetDataDoctorDailyPlanForCalendar.push(setDataDoctorDailyPlanForCalendar);

    $('#DoctorDailyPlanCalendar').fullCalendar({
        header: {
            left: 'prev, next',
            center: 'title',
            right: 'today, month, agendaWeek, agendaDay'
        },
        //Add Events
        events: arrSetDataDoctorDailyPlanForCalendar,
        editable: false,
        eventLimit: true,
        droppable: false
    });
}
function hidClickCalendar() {
    setTimeout(function(){ 
        jQuery('#DoctorDailyPlanCalendar .fc-state-active').click();
    }, 300);
}
// UPDATE PERMISSION BY ID 2020-08-19  START ===========
function Permission(id){
    switch(id){
        case "2" :
                $("li.BindNurse").addClass("d-none");
        break;
        case "3" :
                $("li.BindPhama").addClass("d-none");
        break;
        default:
            break;
    }
}
// UPDATE PERMISSION BY ID 2020-08-19  END ============
function saveDataPatient(getParam,title){
    var saveTxtArea = $('textarea#Alert_New_appointment').val();
    var saveSymptomArea = $('textarea#txt_symptom').val();
    var saveCheckupArea = $('textarea#txt_checkup').val();
    var saveDiagnoseAreaMain = $('textarea#txt_diagnose_main').val();
    var saveTxtFortune = ($('textarea#txt_nofuture').val() != "")? saveTxtFortune : saveTxtFortune = "ไม่ระบุ" ;
    var GetTxtSelected = $( "#select_blood_result option:selected" ).text();
    var saveDiagnoseAreaSpecial = $('textarea#txt_diagnose_special').val(); /////saveDiagnoseAreaSpecial กรอกก็ได้ไม่กรอกก็ได้
    var saveTreatArea = $('textarea#txt_treat').val();
    var cusCheck = jQuery("#chk_remed").prop('checked');
    var typeStr = ""+getParam+"";
        if(jQuery("#chk_remed").prop('checked') == true){
            if(saveTxtArea != ""){
                // METHOD.POST DATA == PatientId,message and TYPE[4]
                let id = sessionStorage.getItem("doctor_id");
                var urlParams = new URLSearchParams(window.location.search);
                var uaid = urlParams.get('uaid');
                var ptid = urlParams.get('ptid');
                var settings = {
                "url": APIservice + "/api/Doctor/SendNotificationToPatient",
                "method": "POST",
                "timeout": 0,
                "headers": {
                "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                "user_account_id":""+uaid+"",
                "doctor_id":""+id+"",
                "patient_id":""+ptid+"",
                "title":""+title+"",
                "message":""+saveTxtArea+"",
                "remark":"",
                "type":""+typeStr+"",
                "new_appointment":cusCheck}),
                // อาการ :"+saveSymptomArea+" ตรวจร่างกาย :"+saveCheckupArea+" วินิจฉัยโรค :"+saveDiagnoseAreaMain+" รักษา : "+saveTreatArea+" เหตุผลนัดหมาย : "+saveTxtArea+" สาเหตุที่ไม่พบแพทย์ : "+saveTxtFortune+"
        };
        $("#txtAlertEditOperation").addClass("d-none");
        $.ajax(settings).done(function (response) {
            $("#SaveDataPatient").modal("hide"); 
        });

            }
            if(saveTxtArea == ""){
                $("#txtAlertSaveOperation").removeClass("d-none");
            }
        }
        if (jQuery("#chk_remed").prop('checked') == false){
           
            // METHOD.POST DATA == PatientId,message and TYPE[4]
            let id = sessionStorage.getItem("doctor_id");
            var urlParams = new URLSearchParams(window.location.search);
            var uaid = urlParams.get('uaid');
            var ptid = urlParams.get('ptid');
            var settings = {
            "url": APIservice + "/api/Doctor/SendNotificationToPatient",
            "method": "POST",
            "timeout": 0,
            "headers": {
            "Content-Type": "application/json"
            },
            "data": JSON.stringify({
            "user_account_id":""+uaid+"",
            "doctor_id":""+id+"",
            "patient_id":""+ptid+"",
            "title":""+title+"",
            "message":"ผลเลือด : "+GetTxtSelected+"",
            "remark":"",
            "type":""+typeStr+"",
            "new_appointment":cusCheck}),
            // อาการ :"+saveSymptomArea+" ตรวจร่างกาย :"+saveCheckupArea+" วินิจฉัยโรค :"+saveDiagnoseAreaMain+" รักษา : "+saveTreatArea+" เหตุผลนัดหมาย : "+saveTxtArea+" สาเหตุที่ไม่พบแพทย์ : "+saveTxtFortune+"// message####
                //+saveSymptomArea+saveCheckupArea+saveDiagnoseAreaMain+saveTreatArea+saveTxtArea+ // message####
        };
        $("#txtAlertEditOperation").addClass("d-none");
        $.ajax(settings).done(function (response) {
            $("#SaveDataPatient").modal("hide"); 
        });
        } 
}
function EditOperation(getParam,title){
    var EditTxtArea = $('textarea#Edit_txt_remed_remark').val();
    var cusCheck = jQuery("#chk_remed").prop('checked');
    var typeStr = ""+getParam+"";
    if(EditTxtArea != ""){
        // METHOD.POST DATA == PatientId,message and TYPE[4]
        let id = sessionStorage.getItem("doctor_id");
        var urlParams = new URLSearchParams(window.location.search);
        var uaid = urlParams.get('uaid');
        var ptid = urlParams.get('ptid');
        var settings = {
        "url": APIservice + "/api/Doctor/SendNotificationToPatient",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json"
        },
        "data": JSON.stringify({
        "user_account_id":""+uaid+"", 
        "doctor_id":""+id+"", 
        "patient_id":""+ptid+"",  
        "title":""+title+"",  
        "message":""+EditTxtArea+"",
        "remark":"",
        "type":""+typeStr+"",
        "new_appointment":true}),
      };
      $("#txtAlertEditOperation").addClass("d-none");
      $.ajax(settings).done(function (response) {
        // console.log("ส่งข้อมูลสำเร็จ ข้อมูล"+JSON.stringify(response));
        $("#Edit-new-appointment").modal("hide"); 
      });

    }
    else {
        $("#txtAlertEditOperation").removeClass("d-none");

    } 
}
function CancelOperation(getParam,title){
    var cancelTxtArea = $('textarea#cancel_txt_remed_remark').val();
    var cusCheck = jQuery("#chk_remed").prop('checked');
    var typeStr = ""+getParam+"";
    if(cancelTxtArea != ""){
        // METHOD.POST DATA == PatientId,message and TYPE[4]
        let id = sessionStorage.getItem("doctor_id");
        var urlParams = new URLSearchParams(window.location.search);
        var uaid = urlParams.get('uaid');
        var ptid = urlParams.get('ptid');
        var settings = {
        "url": APIservice + "/api/Doctor/SendNotificationToPatient",
        "method": "POST",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/json"
        },
        "data": JSON.stringify({
            "user_account_id":""+uaid+"",
            "doctor_id":""+id+"",
            "patient_id":""+ptid+"",
            "title":""+title+"",
            "message":""+cancelTxtArea+"",
            "remark":"",
            "type":""+typeStr+"",
            "new_appointment":cusCheck}),
      };
      $("#txtAlertCancelOperation").addClass("d-none");
      $.ajax(settings).done(function (response) {
        // alert("ส่งข้อมูลสำเร็จ ข้อมูล"+JSON.stringify(response));
        $("#cancel-appointment").modal("hide"); 
      });

    }
    else {
        $("#txtAlertCancelOperation").removeClass("d-none");

    }    
}
function taskOperation(getParam){
    
    var title = "" ;
     switch(getParam){
            case 4 : title = "ยกเลิกนัดหมาย" 
            CancelOperation(getParam,title);   
        break;
            case 2 : title = "นัดหมายใหม่" 
            saveDataPatient(getParam,title);
        break;
            case 3 : title = "เลื่อนนัดหมาย"
            EditOperation(getParam,title);
        break;
            default :
        break;
    } 
}
function ValidationDataPatient(){
    var saveTxtArea = $('textarea#Alert_New_appointment').val();
    var saveSymptomArea = $('textarea#txt_symptom').val();
    var saveCheckupArea = $('textarea#txt_checkup').val();
    var saveDiagnoseAreaMain = $('textarea#txt_diagnose_main').val();
    var saveDiagnoseAreaSpecial = $('textarea#txt_diagnose_special').val();
    var saveTreatArea = $('textarea#txt_treat').val();
    var GetTxtSelected = $( "#select_blood_result option:selected" ).val();
    // VALIDATION TEXTAREA ============================================
    if(saveSymptomArea == "" && saveCheckupArea == "" && saveDiagnoseAreaMain == "" && saveTreatArea == ""){
        $("#Alert_txtArea_symtom").removeClass("d-none");
        $("#Alert_txtArea_checkup").removeClass("d-none");
        $("#Alert_txtArea_diagnose").removeClass("d-none");
        $("#Alert_txtArea_treat").removeClass("d-none");
    }
    // ================================================================
    if(saveSymptomArea != "")
     {
        $("#Alert_txtArea_symtom").removeClass("d-none");
        $("#Alert_txtArea_symtom").removeClass("alert-style");
        $("#Alert_txtArea_symtom_text").addClass("d-none");
        $("#Alert_txtArea_symtom").addClass("bg-white");
    }    
    if(saveCheckupArea != ""){
        $("#Alert_txtArea_checkup").removeClass("d-none");
        $("#Alert_txtArea_checkup").removeClass("alert-style");
        $("#Alert_txtArea_checkup_text").addClass("d-none");
        $("#Alert_txtArea_checkup").addClass("bg-white");
    }
    if(saveDiagnoseAreaMain != ""){
        $("#Alert_txtArea_diagnose").removeClass("d-none");
        $("#Alert_txtArea_diagnose").removeClass("alert-style");
        $("#Alert_txtArea_diagnose_text").addClass("d-none");
        $("#Alert_txtArea_diagnose").addClass("bg-white");
    }
    if(saveTreatArea != ""){
        $("#Alert_txtArea_treat").removeClass("d-none");
        $("#Alert_txtArea_treat").removeClass("alert-style");
        $("#Alert_txtArea_treat_text").addClass("d-none");
        $("#Alert_txtArea_treat").addClass("bg-white");
    }
    if(saveSymptomArea == "" && saveCheckupArea == "" ){
        $("#Alert_txtArea_symtom").addClass("d-none");
        $("#Alert_txtArea_checkup").addClass("d-none");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    if(saveSymptomArea != "" && saveCheckupArea != "")
    {
        $("#Alert_txtArea_symtom").addClass("d-none");
        $("#Alert_txtArea_checkup").addClass("d-none");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    if(saveSymptomArea != "" && saveCheckupArea == "")
    {
        $("#Alert_txtArea_checkup").removeClass("d-none");
        $("#Alert_txtArea_checkup").addClass("alert-style");
        $("#Alert_txtArea_checkup_text").addClass("d-none");
        $("#Alert_txtArea_checkup").removeClass("bg-white");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    if(saveSymptomArea == "" && saveCheckupArea != "")
    {
        $("#Alert_txtArea_symtom").removeClass("d-none");
        $("#Alert_txtArea_symtom").addClass("alert-style");
        $("#Alert_txtArea_symtom_text").addClass("d-none");
        $("#Alert_txtArea_symtom").removeClass("bg-white");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    if(saveSymptomArea == "" && saveCheckupArea == ""){
        $("#Alert_txtArea_symtom").removeClass("d-none");
        $("#Alert_txtArea_symtom").addClass("alert-style");
        $("#Alert_txtArea_symtom_text").addClass("d-none");
        $("#Alert_txtArea_symtom").removeClass("bg-white");
        $("#Alert_txtArea_checkup").removeClass("d-none");
        $("#Alert_txtArea_checkup").addClass("alert-style");
        $("#Alert_txtArea_checkup_text").addClass("d-none");
        $("#Alert_txtArea_checkup").removeClass("bg-white");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    //==================================================
    if(saveDiagnoseAreaMain == "" && saveTreatArea == ""){
        $("#Alert_txtArea_diagnose").addClass("d-none");
        $("#Alert_txtArea_treat").addClass("d-none");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
        
    }
    if(saveDiagnoseAreaMain != "" && saveTreatArea != "")
    {
        $("#Alert_txtArea_diagnose").addClass("d-none");
        $("#Alert_txtArea_treat").addClass("d-none");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    if(saveDiagnoseAreaMain != "" && saveTreatArea == "")
    {
        $("#Alert_txtArea_treat").removeClass("d-none");
        $("#Alert_txtArea_treat").addClass("alert-style");
        $("#Alert_txtArea_treat_text").addClass("d-none");
        $("#Alert_txtArea_treat").removeClass("bg-white");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    if(saveDiagnoseAreaMain == "" && saveTreatArea != "")
    {
        $("#Alert_txtArea_diagnose").removeClass("d-none");
        $("#Alert_txtArea_diagnose").addClass("alert-style");
        $("#Alert_txtArea_diagnose_text").addClass("d-none");
        $("#Alert_txtArea_diagnose").removeClass("bg-white");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    if(saveDiagnoseAreaMain == "" && saveTreatArea == ""){
        $("#Alert_txtArea_diagnose").removeClass("d-none");
        $("#Alert_txtArea_diagnose").addClass("alert-style");
        $("#Alert_txtArea_diagnose_text").addClass("d-none");
        $("#Alert_txtArea_diagnose").removeClass("bg-white");
        $("#Alert_txtArea_treat").removeClass("d-none");
        $("#Alert_txtArea_treat").addClass("alert-style");
        $("#Alert_txtArea_treat_text").addClass("d-none");
        $("#Alert_txtArea_treat").removeClass("bg-white");
        if(GetTxtSelected == "5"){
            $("#Alert_txtArea_dropdown_left").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("d-none");
              $("#Alert_txtArea_dropdown_right").addClass("alert-style");
              $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
              $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
        }
        if(GetTxtSelected == "1"){
            $("#Alert_txtArea_dropdown_left").addClass("d-none");
            $("#Alert_txtArea_dropdown_right").addClass("d-none");
        }
    }
    if(saveSymptomArea != "" && saveCheckupArea != "" && saveDiagnoseAreaMain != "" && saveTreatArea !="" ){
        switch (GetTxtSelected){
            case "1" : 
            $("#SaveDataPatient").modal();
            break;
            case "2" : 
            $("#SaveDataPatient").modal();;
            break;
            case "3" : 
            $("#SaveDataPatient").modal();;
            break;
            case "4" : 
            $("#SaveDataPatient").modal();;
            break;
            case "5" :
                if($("#txt_nofuture").val() == "")
                    {
                        $("#Alert_txtArea_dropdown_left").removeClass("d-none");
                        $("#Alert_txtArea_dropdown_right").removeClass("d-none");
                        $("#Alert_txtArea_dropdown_right").addClass("alert-style");
                        $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
                        $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
                    }if($("#txt_nofuture").val() != "")
                    {
                        $("#Alert_txtArea_dropdown_left").addClass("d-none");
                        $("#Alert_txtArea_dropdown_right").addClass("d-none");
                        $("#SaveDataPatient").modal();
                    }     
            break;
            default:
                break;
        }
    }
    if(saveSymptomArea == "" && saveCheckupArea == "" && saveDiagnoseAreaMain == "" && saveTreatArea == "" ){
        switch (GetTxtSelected){
            case "1" : 
                        $("#Alert_txtArea_dropdown_left").addClass("d-none");
                        $("#Alert_txtArea_dropdown_right").addClass("d-none");
            break;
            case "2" : ;
            break;
            case "3" : ;
            break;
            case "4" : ;
            break;
            case "5" :
                if($("#txt_nofuture").val() == "")
                    {
                        $("#Alert_txtArea_dropdown_left").removeClass("d-none");
                        $("#Alert_txtArea_dropdown_right").removeClass("d-none");
                        $("#Alert_txtArea_dropdown_right").addClass("alert-style");
                        $("#Alert_txtArea_dropdown_right_text").addClass("d-none");
                        $("#Alert_txtArea_dropdown_right").removeClass("bg-white");
                    }     
            break;
            default:
                break;
        }
    }
}

function showNurseProfile() {
    jQuery("#nurse_profile").css("display", "block");
    jQuery("#nurse_fullname").html(jQuery("#slt_nurse").val());
}

// ------------------------ Lab Program ------------------------
var labcode_OD0001 = ["32203","32401","32206","32204","32004","32501","32503","32504","32502","32003","34301","32403","32208","32207","32310","32311","32309","32312","32201","32202","32001","32102","32103","32104","32105","32106","32107","32109","32304","32305","32407","32612","32610","32608","37303","32002","34114","34002","34004","34101","34116","34103","34118","34120","34102","34104","34122","34121","32203","32403","32303","32201","32202","32501","32502","32203","32306","34301","32102","32103","32104","32205","32306","32313","32303","32210","32406","37218","32008","30101","30105","30104","30103","30202","30201","30208","31001","31001","35240","30405"];
var labname_OD0001 = ["Glucose (Blood, Urine, Other) (Quantitative)","Hb A1C","Fructosamine","Oral Glucose Tolerance Test (OGTT) ระดับ plasma glucose 2 ครั้ง","Lipid profile (Cholesterol, HDL-chol, LDL-chol, TG)","Lipid - Cholesterol","Lipid - HDL - cholesterol Cholesterol in HDL","Lipid - LDL - chol (direct) Cholesterol in LDL, Direct assay สั่งรายการเดียว","Lipid - TG (Triglyceride)","Liver function test","Total protein (Quantitative)","Albumin (Quantitative)","Bilirubin, Total","Bilirubin, Direct (Bilirubin.glucuronidated + Bilirubin.albumin bound)","SGOT (AST Aspartate aminotransferase)","SGPT (ALT Alanine aminotransferase)","Alkaline phosphatase","Gamma glutamyl transpeptidase","BUN (Blood Urea Nitrogen)","Creatinine","Electrolyte (Na, K, Cl, CO2)","Sodium","Potassium","Chloride","CO2","Calcium (Serum or Plasma, mg/dL)","Magnesium","Phosphorus (Phosphate)","Creatinine Phosphokinase (CPK) (Creatine kinase) (Quantitative)","Creatine Kinase-MB (CK-MB)","NT-pro BNP (Natriuretic peptide.B prohormone N-Terminal)","Thyroid hormone - Free T3 (Free Tri - iodothyroxine ) (Triiodothyronine.free)","Thyroid hormone - Free T4 (Free Thyroxine)","Thyroid hormone - TSH (Thyroid Stimulating Hormone) (Thyrotropin) (Quantitative)","Beta - HCG (Choriogonadotropin.beta subunit) (Quantitative)","Blood gas analysis","Amylase, Urine","Sodium, Urine","Potassium, Urine","Creatinine, Urine","Microalbumin, Urine","Total protein, Urine random (Quantitative)","Calcium, 24 hr Urine","Phosphorus, 24 hr Urine","Creatinine, 24 hr Urine","Total protein, 24 hr Urine (Quantitative)","Sodium, 24 hr Urine","Potassium, 24 hr Urine","Glucose (Blood, Urine, Other) (Quantitative)","Albumin (Quantitative)","Amylase, Serum","BUN (Blood Urea Nitrogen)","Creatinine","Lipid - Cholesterol","Lipid - TG (Triglyceride)","Glucose (Blood, Urine, Other) (Quantitative)","LDH Lactate dehydrogenase (Quantitative)","Total protein (Quantitative)","Sodium","Potassium","Chloride","Uric acid (Urate)(Quantitative)","LDH Lactate dehydrogenase (Quantitative)","Lipase (Triacylglycerol lipase)","Amylase, Serum","Ammonia","Ceruloplasmin","C-reactive protein (CRP), High sensitivity","Lactate","Complete blood count (CBC)","Erythrocyte Sedimentation Rate (ESR)","Hematocrit (centrifuged)","Reticulocyte count","Partial Thomboplastin Time (PTT)","Prothombin Time (PT) and International Normalize Ratio (INR)","D-dimer (Automate) (Quantitative)","Urinalysis (Physical + Chemical + Microscopic) PANEL.UA","Urinalysis (Physical + Chemical + Microscopic) PANEL.UA","Fungus: Molecular identification","BCR/ABL gene for CML - RT-PCR"];
var labcode_OD0002 = ["31203","37906","31225","31205","35006","36103","36103","36203","31214","31218"];
var labname_OD0002 = ["Occult blood, Stool","Simple sedimentation (Stool)","Cryptosporidium (modified acid fast stain)","Fat stain, Stool (Sudan IV stain)","Microsporidia, special stain","Toxoplasma Ab IgG (Quantitative)","Toxoplasma Ab IgM (Quantitative)","Cysticercosis Ab (Taenia solium larva Ab)","Malaria, thick film","Parasite identification (Ova & parasites identified)"];
var labcode_OD0003 = ["32610","32611","32608","32613","32614","37254","32418","32603","32601","34111","34105","34109","34110","34108","34107","32604","32605","32606","33316","32616","32617","32622","32618","32619","32620","37303","32607","32625","32624","32623","32626","32627","32505","32506","32507","32008","34115","37502","32203","32401","32202","32501","32502","32503","32103","32205","32311","32309","30202","30201","30203","30206","30214","30215","30216","30218","30219","30223","30224","30223","30220","30222","30208","30209","30210","30226","30227","30228","30229","30217","30233","30311","30230","37516","30308","30306","30304","30305","30307","30313","30404","37532","30312","30133"];
var labname_OD0003 = ["Thyroid hormone - Free T4 (Free Thyroxine)","Thyroid hormone - T3 (Tri - iodothyroxine) (Triiodothyronine)","Thyroid hormone - TSH (Thyroid Stimulating Hormone) (Thyrotropin) (Quantitative)","PTH (intact) (Parathyrin.intact)","Osteocalcin","B-crosslaps (Collagen crosslinked C-telopeptide)","Vitamin D (Calciferol) Vitamin D2","Cortisol","ACTH (Corticotropin)","Free cortisol, Urine","Vanillylmandelic acid (VMA), Urine","HVA (Homovanillic acid), Urine (Quantitative)","HIAA, Urine (5-Hydroxyindoleacetic acid) (Quantitative)","Metanephrine, Urine","Catecholamine, Urine (Quantitative)","Aldosterone","Renin","17-OH-progesterone (17-Hydroxyprogesterone Quantitative)","Metanephrine and normetanephrine, plasma","FSH (Follicle stimulating hormone) (Follitropin)","LH (Lutropin)","Prolactin","Estradiol","Progesterone","Testosterone (Quantitative)","Beta - HCG (Choriogonadotropin.beta subunit) (Quantitative)","DHEA-sulphate (Dehydroepiandrosterone sulfate)","Insulin (Quantitative)","C-peptide","Growth hormone (Somatotropin)","Insulin - IGF1 (Insulin-like growth factor-I)","Insulin - IGF BP3 (Insulin-like growth factor binding protein 3) (Quantitative)","Apo lipoprotein A","Apo lipoprotein B","Lipoprotein a","Lactate","Porphyrin, Urine (Quantitative)","Quantitative plasma amino acid analysis","Glucose (Blood, Urine, Other) (Quantitative)","Hb A1C","Creatinine","Lipid - Cholesterol","Lipid - TG (Triglyceride)","Lipid - HDL - cholesterol Cholesterol in HDL","Potassium","Uric acid (Urate)(Quantitative)","SGPT (ALT Alanine aminotransferase)","Alkaline phosphatase","Partial Thomboplastin Time (PTT)","Prothombin Time (PT) and International Normalize Ratio (INR)","Thrombin Time (TT)","Euglobulin Lysis Time (ELT)","Fibrinogen level","Factor assay - Factor II","Factor assay - Factor V","Factor assay - Factor VII","Factor assay - Factor VIII","Factor assay - Factor X","Factor assay - Factor XI","Factor assay - Factor X","Factor assay - Factor VIII Inhibitor (Quantitative)","Factor assay - Factor IX Inhibitor (Quantitative)","D-dimer (Automate) (Quantitative)","Von Willebrand factor (Activity)","Bleeding time","Platelet aggregration (อย่างน้อยต้องมี การตรวจ ADP, Collagen และ Adrenaline)","Protein C (chromogenic assay)","Protein S (chromogenic assay)","Antithrombin III activity (chromogenic)","Activated Protein C Resistance assay","Lupus anticoagulant (screening)","Homocysteine","Heparin anti Xa","Factor V Leiden - DNA analysis","EPO (erythropoietin)","Ferritin","Iron, Serum","TIBC (Iron binding capacity)","Transferrin","Hemoglobin typing (Hb typing) (Hemoglobin electrophoresis panel in Blood)","Thalassemia, deletion (อย่างน้อยตรวจ alpha SEA, THAI, -3.7, -4.2) - Multiplex gap PCR (PANEL.MOLPATH)","Thalassemia, beta mutations","Ham’s test, Acid hemolysis [Presence] of Blood","Osmotic fragility test, quantitative"];
var labcode_OD0004 = ["36642","36752","36553","33506","33508","33110","33517","32108","33503","33701","33709","33707","32411","32415","32409","32413","32410","32416","32417","32412","32414","33555","33556","33557","33550","33552","33554","33558","33107","33108","33123","33171","33102","33101","33104","33302","33103","33602","33803","33604","33605","33201","33203","33204","33208","33005","33006","33123","33114","37239","37308","37310","37306","32615","37313","37311","31001","31002","32002","30104","31201","31203","35002","35001","35005","30110","31301","35003","35004","37506","37576","30402","30402","30402","37509","37513","37508"];
var labname_OD0004 = ["Rabies virus (NASBA) (Nucleic Acid Sequence Based Amplification)","Enterovirus 71 RNA detection","Adenovirus DNA detection (Qualitative)","Copper (quantitative)","Lead (quantitative)","Lithium (quantitative)","Manganese (quantitative)","Zinc, Serum/Urine","Arsenic (quantitative จาก Urine, EDTA blood)","Amphetamine","Methamphetamine, Confirm test (quantitative)","Methadone, Confirm test (quantitative)","Vitamin B12 (Cobalamins)","Folate","Vitamin A (Retinol)","Vitamin E (Tocopherols)","Vitamin B1 (Thiamine)","Vitamin B2 (Riboflavin)","Vitamin B6 (Pyridoxine)","Vitamin C (Ascorbic acid)","Beta carotene","Ethanol (Ethyl alcohol) วิธี GC","Isopropanol (qualitative)","Methanol วิธี GC","Acetone (quantitative)","Benzene (quantitative)","Chloroform","Thinner (Toluene)","Acetaminophen (quantitative)","Salicylate (quantitative)","Benzodiazepine (qualitative)","Phenothiazine (quantitative)","Phenobarbital (quantitative)","Carbamazepine (quantitative)","Valproic acid/Sodium valproate (quantitative)","Digoxin (quantitative)","Phenytoin (quantitative)","Carbamate","Cholinesterase, Plasma or red cell (Quantitative)","Organophosphate (qualitative)","Paraquat (qualitative)","Cyclosporin (quantitative)","Tacrolimus (quantitative)","Sirolimus (quantitative)","Everolimus (quantitative)","Vancomycin (quantitative)","Amikacin (quantitative)","Benzodiazepine (qualitative)","Antidepressants","Antihistamines (qualitative)","CEA (Carcinoembryonic antigen)","PSA (Prostate-specific antigen)","CA 125 (Cancer Ag 125) (Quantitative)","Calcitonin","NSE (Neuron-specific enolase)","Free PSA (Prostate specific Ag.free)","Urinalysis (Physical + Chemical + Microscopic) PANEL.UA","Specific gravity","Blood gas analysis","Hematocrit (centrifuged)","Direct smear, Stool (Blood Cell Count Panel)","Occult blood, Stool","Gram stain","AFB stain (Acid-Fast Bacilli stain)","Modified acid-fast stain","Wright stain, Buffy coat (Differential panel)","Cell count and diff, Body fluid","Indian ink preparation","KOH preparation","Chromosome breakage study","Chromosome analysis in leukemia (Bone marrow/blood)","Chromosome analysis (Amniotic fluid/CVS/Tissue)","Chromosome analysis (Amniotic fluid/CVS/Tissue)","Chromosome analysis (Amniotic fluid/CVS/Tissue)","Fragile X syndrome - Methylation PCR","Spinal muscular atrophy DNA analysis (SMN1 gene)","SRY gene - PCR"];
var labcode_OD0005 = ["36552","36555","36440","36461","36443","36610","36613","36430","36433","36320","36333","36402","36405","31503","36362","36506","36508","36671","36655","36656","36650","36684","36680","36680","36570","36660","36661","36420","36420"];
var labname_OD0005 = ["Adenovirus Ag (Qualitative)","Adenovirus, viral load","Cytomegalovirus (CMV) Ab","Cytomegalovirus (CMV) Ag","Cytomegalovirus (CMV) viral load - Quantitative (Real time PCR)","Dengue virus Ab (qualitative)","Dengue virus, qualitative RT - PCR","Epstein-Barr virus EBV Ab detection","Epstein-Barr virus EBV, viral load RT - PCR","Hepatitis B virus HBV PCR - viral load","Hepatitis C virus HCV viral load - Quantitative (Real time PCR)","HSV-1 and -2 Ab detection","HSV type 1&2 viral load - Quantitative (Real time PCR)","HSV detection - Tzank’s smear (Wright's stain)","HIV viral load - Quantitative (Real time PCR)","Influenza A and B virus Ag (rapid test)","Influenza A virus RNA detection (Quantitative)","JC Virus and BK Virus viral load - Quantitative (Real time PCR)","Measles virus Ab IgG","Measles virus Ab IgM","Mumps Ab IgG (ELISA)","Mumps Ab IgM (ELISA)","Parvo virus B19 Ab IgG (ELISA)","Parvo virus B19 Ab IgM (ELISA)","Rota virus Ag","Rubella Ab IgG","Rubella Ab IgM","Varicella zoster virus (VZV) Ab IgG (ELISA)","Varicella zoster virus (VZV) Ab IgM (ELISA)"];
var labcode_OD0006 = ["35002","35001","35005","31502","31302","35101","35102","35105","35109","36046","36023","36014","36710","35103","36037","36018","35250"];
var labname_OD0006 = ["Gram stain","AFB stain (Acid-Fast Bacilli stain)","Modified acid-fast stain","Treponema pallidum - Dark field examination","Giemsa stain for Virus or Parasite","Aerobic culture and sensitivity","Anaerobic culture and sensitivity","Hemoculture and sensitivity, Automate ต่อ 1 ขวด","Minimum Inhibitory Concentration (MIC)","Mycoplasma pneumoniae, quantitative DNA detection","Mycoplasma pneumoniae Ab","Helicobacter pylori/Ab (Quantitative)","Chlamydophila pneumoniae DNA detection","Mycobacterium culture","Mycobacteria: antimicrobial susceptibility test for 2nd line anti-TB ชื่อยา Kanamycin, Levofloxacin","Mycobacteria: direct PCR","IFN- ᵞ release assay for TB (Mycobacterium tuberculosis stimulated gamma interferon panel)"];
var labcode_OD0007 = ["35003","35004","35107","35116","37227","35240","37106"];
var labname_OD0007 = ["Indian ink preparation","KOH preparation","Culture for fungus","Aspergillus: galactomannan Ag detection (Quantitative)","Cryptococcal Ag, serum/CSF (Qualitative)","Fungus: Molecular identification","Cold agglutinin, (Qualitative)"];
var labcode_OD0008 = ["36311","36317","36331","36350","36318","36004","36006","36003","37302","37304","37307","37101","37105","37107","37003","37004","37008","37005","37009","37010","37001","37211","32630","37207","37208","37016","37017","37233","37234","37015","36351","36314","36317","36319","36315","36311","36331","36302","36385","37103","37219","37218","30621","30601","30622","37355","37201","37206","37350","37261","32006","33154","30611","30609","30612","30616","30627"];
var labname_OD0008 = ["Hepatitis B virus HBc Ab (Hepatitis B virus core Ab)","Hepatitis B virus HBs Ab detection","Hepatitis C virus HCV Ab (Hepatitis C Antibody)","HIV Ab (screening) - RAPID","Hepatitis B virus HBs Ag (Hepatitis B surface antigen) - PHA","Treponema pallidum - FTA - Abs","Treponema pallidum - TPHA","Treponema pallidum - VDRL (RPR) (Reagin Ab, D400","Alpha Fetoprotein (AFP) (Alpha-1-Fetoprotein)","Beta 2 microglobulin, serum/urine (Quantitative)","CA 19-9 (Cancer Ag 19-9) (Quantitative)","Complement C3 level - Latex (Qualitative)","Complement CH50 (Complement total hemolytic CH50, Quantitative)","Complement C4 level (Quantitative)","Antinuclear Ab (FANA, ANA)","Anti-dsDNA Ab (DNA double strand Ab, Quantitaive)","Anti-nRNP Ab (ELISA) (Ribonucleoprotein extractable nuclear Ab Quantitative)","Anti-Sm Ab (Smith extractable nuclear Ab)","Anti-Ro (SS-A) Ab (Sjogrens syndrome-A extractable nuclear Ab, Quantitative)","Anti-La (SS-B) Ab (Sjogrens syndrome-B extractable nuclear Ab Quantitative)","Rheumatoid factor - Latex (Qualitative)","Anti-Thyroglobulin Ab","Thyroglobulin, Serum","Anti-Cardiolipin IgG (Quantitative)","Anti-Cardiolipin IgM (Quantitative)","Antimitochondrial Ab","Anti-Smooth muscle Ab","Anti-Beta-2 glycoprotein 1 IgG (Quantitative)","Anti-Beta-2 glycoprotein 1 IgM  (Quantitative)","Antineutrophil Cytoplasmic Antibodies (ANCA) (Quantitative)","HIV Ab (screening)","Hepatitis B virus Hbe Ag","Hepatitis B virus HBs Ab detection","Hepatitis B virus HBs Ag (Hepatitis B surface antigen) - ELISA, MEIA, ECLIA","Hepatitis B virus Hbe Ab","Hepatitis B virus HBc Ab (Hepatitis B virus core Ab)","Hepatitis C virus HCV Ab (Hepatitis C Antibody)","Hepatitis A virus - Anti HAV IgM (ELISA)","Hepatitis E virus HEV Ab","C-reactive protein (CRP) (Quantitative)","Cryoglobulin (Qualitative)","C-reactive protein (CRP), High sensitivity","HLA-B*1502 allele -Realtime PCR (HLA-B*15:02)","HLA-B27 Serologic typing","HLA-B*5801 allele -Realtime PCR (HLA-B*58:01)","Specific IgE, quantitative (1 allergen)","Immunoglobulin level IgG (Quantitative)","Immunoglobulin level IgM (Quantitative)","Immunoglobulin level IgE (total)","IgK (Immunoglobulin kappa light chain gene) Rearrangement - Multiplex PCR","Protein electrophoresis, serum/urine","Immunofixation electrophoresis","HLA class II DNA low resolution (DRB, DQB) typing","HLA - A, B DNA typing","HLA - A DNA typing (low resolution)","HLA - B DNA typing (low resolution)","Specific PRA HLA Class II - Luminex (HLA-DP+DQ+DR Ab)"];
var labcode_OD0009 = ["RD001","RD002","RD003","RD004","RD005","RD006","RD007","RD008","RD009","RD010","RD011","RD012","RD013","RD014","RD015","RD017"];
var labname_OD0009 = ["Skull sries (Caldwell, Towne, Lat)","Orbits (Waters, Caldwell, opic foreman","Moving eye balls","Facial bones (Waters Caldweel)","Neck (AP, Lat)","TMJ (close & open)","IACs (Stenvers,PA,Towne","Mastoid (Law,Tangential)","Nasopharynx(Lat,base)","Styloid process (Towne,Lat)","mandible (AP,both obl)","Panoramic mandible","Cephalagram (Lat)","Nasal bone (Waters,Lat-soft tissue)","paranasal sinuses (3viwes)","Other..."];
var labcode_OD0010 = ["RD016","RD021","RD022","RD023","RD024","RD025","RD026","RD027"];
var labname_OD0010 = ["Chest PA","Chest left laleral","Rib ( Chest AP,Obi)","Chest (lateral Decubitus) RL,Lt","Chest (Lordotic/Apicogram)","Chest(Portable)","Chest (ในสถานที่,ไปต่างประเทศ)","Chest (นอกสถานที่)"];

var setchkbox = [6,5,5,5,5,5,5,5,6,5];

function setDataLabProgram(labid) {
    var setStrData = "";
    var labcode = eval("labcode_" + labid);
    var labname = eval("labname_" + labid);

    var nolabid = labid.replace("OD", "");

    for(var i=1; i<=labcode.length; i++) {
        setStrData += "<div id='listLab_" + i + "' class='row gutters'>";
        setStrData += "<div class='col-xl-1 col-lg-1 col-md-1 col-sm-12 col-12' style='text-align: center;'>";
        setStrData += "<div class='custom-control custom-checkbox'>";
        if (i <= setchkbox[parseInt(nolabid)-1]) {
            setStrData += "<input type='checkbox' class='custom-control-input' style='cursor: pointer;' id='chk_list_lab_" + i + "' checked='checked'>";
            setStrData += "<label class='custom-control-label' style='cursor: pointer;' for='chk_list_lab_" + i + "'></label>";
        } else {
            setStrData += "<input type='checkbox' class='custom-control-input' style='cursor: pointer;' id='chk_list_lab_" + i + "'>";
            setStrData += "<label class='custom-control-label' style='cursor: pointer;' for='chk_list_lab_" + i + "'></label>";
        }
        setStrData += "</div>";
        setStrData += "</div>";
        setStrData += "<div class='col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12' style='text-align: center;' id='txt_labcode_" + i + "'>";
        setStrData += "<span>" + labcode[i-1] + "</span>";
        setStrData += "</div>";
        setStrData += "<div class='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12' style='text-align: left;' id='txt_labname_" + i + "'>";
        setStrData += "<span>" + labname[i-1] + "</span>";
        setStrData += "</div>";
        setStrData += "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12' style='text-align: right;' id='action_" + i + "'>";
        setStrData += "<a href='javascript:void(0);' onclick='edit_list_lab(" + i +  ");'>";
        setStrData += "<span class='icon-edit-2' style='font-size: 24px;'></span>";
        setStrData += "</a>";
        setStrData += "<a href='javascript:void(0);' onclick='delete_list_lab(" + i +  ");'>";
        setStrData += "<span class='icon-delete' style='font-size: 24px;'></span>";
        setStrData += "</a>";
        setStrData += "</div>";
        setStrData += "</div>";  
    }

    jQuery("#list_lab_details").html(setStrData);
}

function add_list_lab() {
    var tempListLab = jQuery("#list_lab_details").html();
    var countListLab = jQuery("#list_lab_details .row").length;
    var setStrData = "";

    jQuery("#list_lab_details .row").each(function(index) {
        if (countListLab == (index + 1)) {
            var getid = jQuery(this)[0].id.replace("listLab_", "");
            countListLab = parseInt(getid);

            setStrData += "<div id='listLab_" + (countListLab + 1) + "' class='row gutters'>";
            setStrData += "<div class='col-xl-1 col-lg-1 col-md-1 col-sm-12 col-12' style='text-align: center;'>";
            setStrData += "<div class='custom-control custom-checkbox'>";
            setStrData += "</div>";
            setStrData += "</div>";
            setStrData += "<div class='col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12' style='text-align: center;' id='txt_labcode_" + (countListLab + 1) + "'>";
            setStrData += "<input type='text' value='' style='width: 100%; padding: 0px 3px; text-align: center;' />";
            setStrData += "</div>";
            setStrData += "<div class='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12' style='text-align: left;' id='txt_labname_" + (countListLab + 1) + "'>";
            setStrData += "<input type='text' value='' style='width: 100%; padding: 0px 3px;' />";
            setStrData += "</div>";
            setStrData += "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12' style='text-align: right;' id='action_" + (countListLab + 1) + "'>";
            setStrData += "<a href='javascript:void(0);' onclick='delete_list_lab(" + (countListLab + 1) +  ");'>";
            setStrData += "<span class='icon-x' style='font-size: 24px;'></span>";
            setStrData += "</a>";
            setStrData += "<a href='javascript:void(0);' onclick='save_list_lab(" + (countListLab + 1) +  ");'>";
            setStrData += "<span class='icon-save' style='font-size: 24px;'></span>";
            setStrData += "</a>";
            setStrData += "</div>";
            setStrData += "</div>";

            jQuery("#list_lab_details").html(tempListLab + setStrData);

            jQuery("#txt_labcode_" + (countListLab + 1) + " input").focus();
        }
    });
}

function edit_list_lab(row) {
    var gettxt_labcode = jQuery("#txt_labcode_" + row + " span").html();
    var gettxt_labname = jQuery("#txt_labname_" + row + " span").html();

    var setAction = "<a href='javascript:void(0);' onclick='delete_list_lab(" + row +  ");'>";
    setAction += "<span class='icon-x' style='font-size: 24px;'></span></a>";
    setAction += "<a href='javascript:void(0);' onclick='save_list_lab(" + row +  ");'>";
    setAction += "<span class='icon-save' style='font-size: 24px;'></span></a>";

    jQuery("#listLab_" + row + " .custom-checkbox").html("");

    jQuery("#txt_labcode_" + row + " span").remove();
    jQuery("#txt_labname_" + row + " span").remove();
    jQuery("#txt_labcode_" + row).html("<input type='text' value='" + gettxt_labcode + "' style='width: 100%; padding: 0px 3px; text-align: center;' />");
    jQuery("#txt_labname_" + row).html("<input type='text' value='" + gettxt_labname + "' style='width: 100%; padding: 0px 3px;' />");

    jQuery("#action_" + row).html("");
    jQuery("#action_" + row).html(setAction);
}

function save_list_lab(row) {
    var gettxt_labcode = jQuery("#txt_labcode_" + row + " input").val();
    var gettxt_labname = jQuery("#txt_labname_" + row + " input").val();

    if (gettxt_labcode != "" && gettxt_labname != "") {
        var setStrChkbox = "<input type='checkbox' class='custom-control-input' style='cursor: pointer;' id='chk_list_lab_" + row + "'>";
        setStrChkbox += "<label class='custom-control-label' style='cursor: pointer;' for='chk_list_lab_" + row + "'></label>";

        var setAction = "<a href='javascript:void(0);' onclick='edit_list_lab(" + row +  ");'>";
        setAction += "<span class='icon-edit-2' style='font-size: 24px;'></span></a>";
        setAction += "<a href='javascript:void(0);' onclick='delete_list_lab(" + row +  ");'>";
        setAction += "<span class='icon-delete' style='font-size: 24px;'></span></a>";

        jQuery("#listLab_" + row + " .custom-checkbox").html(setStrChkbox);

        jQuery("#txt_labcode_" + row + " input").remove();
        jQuery("#txt_labname_" + row + " input").remove();
        jQuery("#txt_labcode_" + row).html("<span>" + gettxt_labcode + "</span>");
        jQuery("#txt_labname_" + row).html("<span>" + gettxt_labname + "</span>");

        jQuery("#action_" + row).html("");
        jQuery("#action_" + row).html(setAction);
    }
}

function delete_list_lab(row) {
    jQuery("#listLab_" + row).remove();
}
// ------------------------ Lab Program ------------------------



function setShowImagesMerdicalCertificate(n) {
    var setStringShowImages = "";
    var setColtext = "";
    var allImages = 5;
    var classImages = "";

    if (n == 4) {
        setColtext = "6";
        classImages = "img_row2";
    } else {
        setColtext = "4";
        classImages = "img_row3";
    }

    //setStringShowImages += "<div class='row gutters'>";
    for (var j = 1; j <= n; j++) {
        if (j <= allImages) {
            //setStringShowImages += "<div class='col-xl-" + setColtext + " col-lg-" + setColtext + " col-md-" + setColtext + " col-sm-12 col-12'>";
            setStringShowImages += "<a href='/images/1.png' class='effects text-center'>";
            setStringShowImages += "<img src='/images/img_mri1.png' class='img-fluid " + classImages + "' style='object-fit: cover;' />";
            setStringShowImages += "<div class='overlay'>";
            setStringShowImages += "<span class='expand'>+</span>";
            setStringShowImages += "</div>";
            setStringShowImages += "</a>";
            //setStringShowImages += "</div>";

            //setStringShowImages += "<a href='/images/1.png' class='btn btn-primary'>พิมพ์ใบรับรองแพทย์</a>";
            //setStringShowImages += "<a href='javascript:void(0);' class='btn btn-primary float-right' style='margin-right: 15px;' data-toggle='modal' data-target='.save-data-patient' id='saveDataPatient'>ดึงข้อมูลใบรับรองแพทย์</a>";
            
        }
    }
    //setStringShowImages += "</div>";

    jQuery("#setshowimages").html(setStringShowImages);
    onloadGallery();
}
